<?php

//---------------------------------- //ENVIRONMENT //--------------------------------//

	if(!function_exists('is_cli')){
	    function is_cli(){
	        return php_sapi_name() === 'cli';
	    }
	}

//---------------------------------- //DEBUGGING //--------------------------------//

	if(!function_exists('pr')){

	    //PRINT OUT DATA
	    function pr($data = null, $string = false){

	        $key = !$string ? 0 : $string;

	        $trace = debug_backtrace()[$key];
	        if($string) ob_start();

	        if(is_cli()){
	            echo $key ? "\r\n" : '';
	            echo "File: ".$trace['file'].' Line: '.$trace['line']."\r\n";
	            !$data ? var_dump($data) : print_r($data);
	            echo "\r\n\r\n";
	        }
	        else{
	            echo '<div class="debug-printer" style="display:block; clear:both;"><b>File: </b>'.$trace['file'].' <b>Line: </b>'.$trace['line'];
	            echo '<pre>'; !$data ? var_dump($data) : print_r($string ? htmlentities_recursive($data) : $data);echo '</pre></div>';
	        }

	        if($string) return ob_get_clean();
	    }
	}

	
	if(!function_exists('px')){

	    //PRINT OUT DATA
	    function px($data = null, $string = false){

	        $trace = debug_backtrace()[0];
	        ob_start();

	        if(is_cli()){
	            echo "File: ".$trace['file'].' Line: '.$trace['line']."\r\n";
	            !$data ? var_dump($data) : print_r($data);
	            echo "\r\n\r\n";
	        }
	        else{
	            echo '<div class="debug-printer" style="display:block; clear:both;"><b>File: </b>'.$trace['file'].' <b>Line: </b>'.$trace['line'];
	            echo '<pre>'; !$data ? var_dump($data) : print_r(htmlentities_recursive($data));echo '</pre></div>';
	        }
	        
	        die(ob_get_clean());
	    }
	}
	

//---------------------------------- //FORMATTING //--------------------------------//

	if(!function_exists('humanFileSize')){
	    function humanFileSize($size,$unit="") {
			if((!$unit && $size >= 1<<30) || $unit == "GB") return number_format($size/(1<<30),2)."GB";
			if((!$unit && $size >= 1<<20) || $unit == "MB") return number_format($size/(1<<20),2)."MB";
			if((!$unit && $size >= 1<<10) || $unit == "KB") return number_format($size/(1<<10),2)."KB";
			return number_format($size)." bytes";
	    }
	}

	if(!function_exists('htmlentities_recursive')){
		function htmlentities_recursive($code) {
			if(is_array($code) || is_object($code)){
				try{
					foreach ($code as &$c) $c = htmlentities_recursive($c);
				}
				catch(\Throwable $e){
					//SILENT
				}
				return $code;
			}
			return htmlentities($code);
		}
	}

	//STRIP OUT INVALID CHARACTERS
	if(!function_exists('cleanNonAsciiCharactersInString')){
		function cleanNonAsciiCharactersInString($orig_text) {

			/*
				CONVERSION REFERENCE:
				https://en.wikipedia.org/wiki/List_of_XML_and_HTML_character_entity_references
				$text = str_replace('À', '&#192', $text);

				THE ABOVE REPLACEMENT SHOULD WORK AS LONG AS THE TRANSLATION IS SET TO HTML_SPECIALCHARS:
				$trans = get_html_translation_table(HTML_SPECIALCHARS);

				REVISIT THIS IF CHARACTERS STILL HAVE ISSUES
			*/

		    $text = $orig_text;

		    // Single letters
		    $text = preg_replace("/[∂άαáàâãªä]/u",      "a", $text);
		    $text = preg_replace("/[∆лДΛдАÁÀÂÃÄ]/u",     "A", $text);
		    $text = preg_replace("/[ЂЪЬБъь]/u",           "b", $text);
		    $text = preg_replace("/[βвВ]/u",            "B", $text);
		    $text = preg_replace("/[çς©с]/u",            "c", $text);
		    $text = preg_replace("/[ÇС]/u",              "C", $text);        
		    $text = preg_replace("/[δ]/u",             "d", $text);
		    $text = preg_replace("/[éèêëέëèεе℮ёєэЭ]/u", "e", $text);
		    $text = preg_replace("/[ÉÈÊË€ξЄ€Е∑]/u",     "E", $text);
		    $text = preg_replace("/[₣]/u",               "F", $text);
		    $text = preg_replace("/[НнЊњ]/u",           "H", $text);
		    $text = preg_replace("/[ђћЋ]/u",            "h", $text);
		    $text = preg_replace("/[ÍÌÎÏ]/u",           "I", $text);
		    $text = preg_replace("/[íìîïιίϊі]/u",       "i", $text);
		    $text = preg_replace("/[Јј]/u",             "j", $text);
		    $text = preg_replace("/[ΚЌК]/u",            'K', $text);
		    $text = preg_replace("/[ќк]/u",             'k', $text);
		    $text = preg_replace("/[ℓ∟]/u",             'l', $text);
		    $text = preg_replace("/[Мм]/u",             "M", $text);
		    $text = preg_replace("/[ñηήηπⁿ]/u",            "n", $text);
		    $text = preg_replace("/[Ñ∏пПИЙийΝЛ]/u",       "N", $text);
		    $text = preg_replace("/[óòôõºöοФσόо]/u", "o", $text);
		    $text = preg_replace("/[ÓÒÔÕÖθΩθОΩ]/u",     "O", $text);
		    $text = preg_replace("/[ρφрРф]/u",          "p", $text);
		    $text = preg_replace("/[®яЯ]/u",              "R", $text); 
		    $text = preg_replace("/[ГЃгѓ]/u",              "r", $text); 
		    $text = preg_replace("/[Ѕ]/u",              "S", $text);
		    $text = preg_replace("/[ѕ]/u",              "s", $text);
		    $text = preg_replace("/[Тт]/u",              "T", $text);
		    $text = preg_replace("/[τ†‡]/u",              "t", $text);
		    $text = preg_replace("/[úùûüџμΰµυϋύ]/u",     "u", $text);
		    $text = preg_replace("/[√]/u",               "v", $text);
		    $text = preg_replace("/[ÚÙÛÜЏЦц]/u",         "U", $text);
		    $text = preg_replace("/[Ψψωώẅẃẁщш]/u",      "w", $text);
		    $text = preg_replace("/[ẀẄẂШЩ]/u",          "W", $text);
		    $text = preg_replace("/[ΧχЖХж]/u",          "x", $text);
		    $text = preg_replace("/[ỲΫ¥]/u",           "Y", $text);
		    $text = preg_replace("/[ỳγўЎУуч]/u",       "y", $text);
		    $text = preg_replace("/[ζ]/u",              "Z", $text);

		    // Punctuation
		    $text = preg_replace("/[‚‚]/u", ",", $text);        
		    $text = preg_replace("/[`‛′’‘]/u", "'", $text);
		    $text = preg_replace("/[″“”«»„]/u", '"', $text);
		    $text = preg_replace("/[—–―−–‾⌐─↔→←]/u", '-', $text);
		    $text = preg_replace("/[  ]/u", ' ', $text);

		    $text = str_replace("…", "...", $text);
		    $text = str_replace("≠", "!=", $text);
		    $text = str_replace("≤", "<=", $text);
		    $text = str_replace("≥", ">=", $text);
		    $text = preg_replace("/[‗≈≡]/u", "=", $text);


		    // Exciting combinations    
		    $text = str_replace("ыЫ", "bl", $text);
		    $text = str_replace("℅", "c/o", $text);
		    $text = str_replace("₧", "Pts", $text);
		    $text = str_replace("™", "tm", $text);
		    $text = str_replace("№", "No", $text);        
		    $text = str_replace("Ч", "4", $text);                
		    $text = str_replace("‰", "%", $text);
		    $text = preg_replace("/[∙•]/u", "*", $text);
		    $text = str_replace("‹", "<", $text);
		    $text = str_replace("›", ">", $text);
		    $text = str_replace("‼", "!!", $text);
		    $text = str_replace("⁄", "/", $text);
		    $text = str_replace("∕", "/", $text);
		    $text = str_replace("⅞", "7/8", $text);
		    $text = str_replace("⅝", "5/8", $text);
		    $text = str_replace("⅜", "3/8", $text);
		    $text = str_replace("⅛", "1/8", $text);        
		    $text = preg_replace("/[‰]/u", "%", $text);
		    $text = preg_replace("/[Љљ]/u", "Ab", $text);
		    $text = preg_replace("/[Юю]/u", "IO", $text);
		    $text = preg_replace("/[ﬁﬂ]/u", "fi", $text);
		    $text = preg_replace("/[зЗ]/u", "3", $text); 
		    $text = str_replace("£", "(pounds)", $text);
		    $text = str_replace("₤", "(lira)", $text);
		    $text = preg_replace("/[‰]/u", "%", $text);
		    $text = preg_replace("/[↨↕↓↑│]/u", "|", $text);
		    $text = preg_replace("/[∞∩∫⌂⌠⌡]/u", "", $text);

		    //2) Translation CP1252.
		    $trans = get_html_translation_table(HTML_ENTITIES);
		    $trans['f'] = '&fnof;';    // Latin Small Letter F With Hook
		    $trans['-'] = array(
		        '&hellip;',     // Horizontal Ellipsis
		        '&tilde;',      // Small Tilde
		        '&ndash;'       // Dash
		        );
		    $trans["+"] = '&dagger;';    // Dagger
		    $trans['#'] = '&Dagger;';    // Double Dagger         
		    $trans['M'] = '&permil;';    // Per Mille Sign
		    $trans['S'] = '&Scaron;';    // Latin Capital Letter S With Caron        
		    $trans['OE'] = '&OElig;';    // Latin Capital Ligature OE
		    $trans["'"] = array(
		        '&lsquo;',  // Left Single Quotation Mark
		        '&rsquo;',  // Right Single Quotation Mark
		        '&rsaquo;', // Single Right-Pointing Angle Quotation Mark
		        '&sbquo;',  // Single Low-9 Quotation Mark
		        '&circ;',   // Modifier Letter Circumflex Accent
		        '&lsaquo;'  // Single Left-Pointing Angle Quotation Mark
		        );

		    $trans['"'] = array(
		        '&ldquo;',  // Left Double Quotation Mark
		        '&rdquo;',  // Right Double Quotation Mark
		        '&bdquo;',  // Double Low-9 Quotation Mark
		        );

		    $trans['*'] = '&bull;';    // Bullet
		    $trans['n'] = '&ndash;';    // En Dash
		    $trans['m'] = '&mdash;';    // Em Dash        
		    $trans['tm'] = '&trade;';    // Trade Mark Sign
		    $trans['s'] = '&scaron;';    // Latin Small Letter S With Caron
		    $trans['oe'] = '&oelig;';    // Latin Small Ligature OE
		    $trans['Y'] = '&Yuml;';    // Latin Capital Letter Y With Diaeresis
		    $trans['euro'] = '&euro;';    // euro currency symbol
		    ksort($trans);

		    foreach ($trans as $k => $v) {
		        $text = str_replace($v, $k, $text);
		    }

		    // 3) remove <p>, <br/> ...
		    $text = strip_tags($text);

		    // 4) &amp; => & &quot; => '
		    $text = html_entity_decode($text);

		    // transliterate
		    // if (function_exists('iconv')) {
		    // $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		    // }

		    // remove non ascii characters
		    // $text =  preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $text);      

		    return $text;
		}
	}


//---------------------------------- // SLICK PHONE //--------------------------------//

	//PHONE FORMATTING CLASS
	if(!function_exists('slickPhone')){
	    function slickPhone($number){

	        //PARSE THE PHONE NUMBER VIA REGEX
	        preg_match("/^\+?(?'prefix'[\d]{1,2})?\(?(?'area'[0-9][\d]{2})\)?[\s|-]?(?'part1'[\d]{3})[\s|-]?(?'part2'[\d]{4})/", $number, $m);

	        //FILTER OUT EMPTY VALUES
	        $m = array_filter($m);

	        //START A NEW DYNAMIC CLASS
	        return new class(['prefix' => @$m['prefix'] ?? null, 'area' => @$m['area'] ?? null, 'part1' => @$m['part1'] ?? null, 'part2' => @$m['part2'] ?? null, 'original' => $number]) extends \GeneralObject {

	            //INITIALIZE THE CLASS
	            public function __construct($data){
	                
	                //CALL PARENT CONSTRUCTOR
	                parent::__construct($data);

	                //SET HELPER VALUES
	                $this->formatted    = $this->__toString();
	                $this->isValid      = $this->_isValid();
	            }

	            //RENDER THIS CLASS A STRING
	            public function __toString(){
	                return($this->prefix ?? '').($this->area ? '('.$this->area.') ' : '').($this->part1 ? $this->part1.'-' : '').($this->part2);
	            }

	            //CHECK IF PROVIDED PHONE NUMBER IS VALID
	            private function _isValid(){
	                if(is_string($this->original) && strlen(trim($this->original)) >= 10) return preg_match("/^\+?([\d]{1,2})?\(?([0-9][\d]{2})\)?[\s|-]?([\d]{3})[\s|-]?([\d]{4})/", $this->original);
	                return false;
	            }
	        };
	    }
	}

	//CHECK IF A PHONE NUMBER IS VALID
	if(!function_exists('phoneIsValid')){    
	    function phoneIsValid($number){
	        return slickPhone($number)->isValid;
	    }
	}

	//FORMAT A PHONE NUMBER
	if(!function_exists('format_phone')){
	    function format_phone($number){
	        return slickPhone($number)->formatted;
	    }
	}

//---------------------------------- // END SLICK PHONE //--------------------------------//


//---------------------------------- // SLICK ARRAY //--------------------------------//

	//GET AN ARRAY VALUE FROM DOT CONCATENATION
	if(!function_exists('getArrayByPath')){
	    function getArrayByPath(array $a, $path, $default = null) {
	        $current    = $a;
	        $p          = strtok($path, '.');
	        while($p !== false){
	            if(!isset($current[$p])) return $default;
	            $current    = $current[$p];
	            $p          = strtok('.');
	        }
	        return $current;
	    }
	}

	//SET AN ARRAY VALUE BY DOT CONCATENATION
	if(!function_exists('assignArrayByPath')){
	    function assignArrayByPath(&$arr, $path, $value, $separator='.') {
	        $keys = explode($separator, $path);
	        foreach ($keys as $key) $arr = &$arr[$key];
	        $arr = $value;
	    }
	}

	//FLATTEN A MULTIDIMENSIONAL ARRAY
	if(!function_exists('flattenArray')){
	    function flattenArray($array, $prefix = ''){
	        $result = [];
	        foreach ($array as $key => $value){
	            $new_key = $prefix . (empty($prefix) ? '' : '.') . $key;
	            if(is_array($value)) $result = array_merge($result, flattenArray($value, $new_key));
	            else $result[$new_key] = $value;
	        }
	        return $result;
	    }
	}

	//EXPAND AN ARRAY INTO MULTIDIMENSIONAL ARRAY
	if(!function_exists('expandArrayPaths')){
	    function expandArrayPaths($arr) {

	        $result = [];
	        foreach($arr as $key => $value) {
	            if (is_array($value)) $value = expandArrayPaths($value);
	            foreach(array_reverse(explode(".", $key)) as $key) $value = [$key => $value];
	            $result = array_merge_recursive($result, $value);
	        }
	        return $result;
	    }
	}

	//ALLOW THE CLASS TO BE CALLED WITHOUT A NAMESPACE
	if(!function_exists('slickArray')){
		function slickArray(&$arr){
			return new \slickArray($arr);
		}
	}

	//ADD ARRAY FUNCTIONALITY CLASS
	if(!class_exists('slickArray')){
		class slickArray implements JsonSerializable,Countable{

			protected $_arr;

			public function __construct(&$array){
				$this->_arr = &$array;
			}

			public function __toString(){
				return json_encode($this->_arr, JSON_PRETTY_PRINT);
			}

			public function &toArray(){
				return $this->_arr;
			}

			public function &expand(){
				return expandArrayPaths($this->_arr);
			}

			public function find(string $path){
				return getArrayByPath($this->_arr, $path);
			}

			public function &set(string $path, $value){
				assignArrayByPath($this->_arr, $path, $value);
				return $this;
			}

			public function flatten(){
				return flattenArray($this->_arr);
			}

			public function export(){
				ob_start();
				var_export($this->_arr);
				return ob_get_clean();
			}

			//--------------------------------------// JsonSerializable INTERFACE METHODS //--------------------------------------//

			public function jsonSerialize(){
				return $this->_arr;
			}

			//--------------------------------------// Countable INTERFACE METHODS //--------------------------------------//

		    //COUNT THE AMOUNT OF DATA
		    public function count(){
		    	return count(array_filter($this->_data, 'strlen'));
		    }
		}
	}


//---------------------------------- // END SLICK ARRAY //--------------------------------//

//---------------------------------- // SLICK State Names //--------------------------------//

if(!class_exists('SlickStateNames')){

    if(!function_exists('slickStateNames')){
        function slickStateNames(string $name = null){
            return new \SlickStateNames($name);
        }
    }

    class SlickStateNames {

        private $_raw;
        private $_short_name;
        private $_long_name;
        private $_states = [
            'AL' => 'ALABAMA',
            'AK' => 'ALASKA',
            'AZ' => 'ARIZONA',
            'AR' => 'ARKANSAS',
            'CA' => 'CALIFORNIA',
            'CO' => 'COLORADO',
            'CT' => 'CONNECTICUT',
            'DC' => 'DISTRICT OF COLUMBIA',
            'DE' => 'DELAWARE',
            'FL' => 'FLORIDA',
            'GA' => 'GEORGIA',
            'GU' => 'GUAM',
            'HI' => 'HAWAII',
            'ID' => 'IDAHO',
            'IL' => 'ILLINOIS',
            'IN' => 'INDIANA',
            'IA' => 'IOWA',
            'KS' => 'KANSAS',
            'KY' => 'KENTUCKY',
            'LA' => 'LOUISIANA',
            'ME' => 'MAINE',
            'MD' => 'MARYLAND',
            'MA' => 'MASSACHUSETTS',
            'MI' => 'MICHIGAN',
            'MN' => 'MINNESOTA',
            'MS' => 'MISSISSIPPI',
            'MO' => 'MISSOURI',
            'MT' => 'MONTANA',
            'NE' => 'NEBRASKA',
            'NV' => 'NEVADA',
            'NH' => 'NEW HAMPSHIRE',
            'NJ' => 'NEW JERSEY',
            'NM' => 'NEW MEXICO',
            'NY' => 'NEW YORK',
            'NC' => 'NORTH CAROLINA',
            'ND' => 'NORTH DAKOTA',
            'OH' => 'OHIO',
            'OK' => 'OKLAHOMA',
            'OR' => 'OREGON',
            'PW' => 'PALAU',
            'PA' => 'PENNSYLVANIA',
            'PR' => 'PUERTO RICO',
            'RI' => 'RHODE ISLAND',
            'SC' => 'SOUTH CAROLINA',
            'SD' => 'SOUTH DAKOTA',
            'TN' => 'TENNESSEE',
            'TX' => 'TEXAS',
            'UT' => 'UTAH',
            'VT' => 'VERMONT',
            'VI' => 'VIRGIN ISLANDS',
            'VA' => 'VIRGINIA',
            'WA' => 'WASHINGTON',
            'WV' => 'WEST VIRGINIA',
            'WI' => 'WISCONSIN',
            'WY' => 'WYOMING',
        ];

        public function __call($name, $arguments = []){

            //GET THE METHOD NAME
            $methodCheck = '_'.$name;

            //CALL THE METHOD
            if(method_exists($this, $methodCheck)) return call_user_func_array([$this, $methodCheck], $arguments);
        }

        public static function __callStatic($name, $arguments = []){

            //GET A NEW VERSION OF THIS CLASS WITHOUT THE CONSTRUCTOR (NEEDED TO BYPASS REQUIRED CONSTRUCTOR VALUES)
            $className  = '\\'.get_called_class();
            $reflection = new \ReflectionClass($className);
            $obj        = $reflection->newInstanceWithoutConstructor();

            //MAKE SURE THIS IS INSTANTIATED
            //$obj = new $className;

            //GET THE METHOD NAME
            $methodCheck = '_'.$name;

            //CALL THE METHOD
            if(method_exists($obj, $methodCheck)) return call_user_func_array([$obj, $methodCheck], $arguments);
        }

        public function __construct(string $string = null){
            if(!is_null($string)) $this->_parse($string);
        }

        public function __toString(){
            return $this->_raw;
        }

        private function _all(){
            return $this->_states;
        }

        private function _parse(string $name){

            $this->_raw = $name;
            if(strlen(trim($name)) == 2) $this->_short_name = strtoupper(trim($name));
            else $this->_long_name = trim($name);

            if(is_null($this->_long_name)) $this->_long_name = ucwords(strtolower($this->_states[$this->_short_name]));
            if(is_null($this->_short_name)) $this->_short_name = array_search(strtoupper($this->_long_name), $this->_states);
            return $this;
        }

        public function _short(){
            return $this->_short_name;
        }

        public function _long(){
            return $this->_long_name;
        }
    }
}

//---------------------------------- // END SLICK State Names //--------------------------------//


//---------------------------------- //CALCULATIONS //--------------------------------//

	if(!function_exists('generateRandomString')){
		function generateRandomString($length = 10) {
		    $characters 		= '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength 	= strlen($characters);
		    $randomString 		= '';
		    for($i = 0; $i < $length; $i++) $randomString .= $characters[rand(0, $charactersLength - 1)];
		    return $randomString;
		}
	}

//---------------------------------- // MIME TYPES //--------------------------------//	

	if(!function_exists('checkMimeType')){
		function checkMimeType($string = null){
			return new \CheckMimeType($string);
		}
	}

	if(!class_exists('CheckMimeType')){
		class CheckMimeType {

			private $_raw;
			private $_extVal;
			private $_mimeVal;

			private $_extensions = [
				'ez'				=> 'application/andrew-inset',
				'atom'				=> 'application/atom+xml',
				'atomcat'			=> 'application/atomcat+xml',
				'atomsvc'			=> 'application/atomsvc+xml',
				'apxml'				=> 'application/auth-policy+xml',
				'ccxml'				=> 'application/ccxml+xml',
				'cellml'			=> 'application/cellml+xml',
				'cml'				=> 'application/cellml+xml',
				'cpl'				=> 'application/cpl+xml',
				'davmount'			=> 'application/davmount+xml',
				'dcm'				=> 'application/dicom',
				'dssc'				=> 'application/dssc+der',
				'xdssc'				=> 'application/dssc+xml',
				'dvc'				=> 'application/dvcs',
				'emma'				=> 'application/emma+xml',
				'finf'				=> 'application/fastinfoset',
				'pfr'				=> 'application/font-tdpfr',
				'stk'				=> 'application/hyperstudio',
				'ipfix'				=> 'application/ipfix',
				'json'				=> 'application/json',
				'lostxml'			=> 'application/lost+xml',
				'hqx'				=> 'application/mac-binhex40',
				'mrc'				=> 'application/marc',
				'nb'				=> 'application/mathematica',
				'ma'				=> 'application/mathematica',
				'mb'				=> 'application/mathematica',
				'mbox'				=> 'application/mbox',
				'm21'				=> 'application/mp21',
				'mp21'				=> 'application/mp21',
				'doc'				=> 'application/msword',
				'mxf'				=> 'application/mxf',
				'orq'				=> 'application/ocsp-request',
				'ors'				=> 'application/ocsp-response',
				'bin'				=> 'application/octet-stream',
				'lha'				=> 'application/octet-stream',
				'lzh'				=> 'application/octet-stream',
				'exe'				=> 'application/octet-stream',
				'class'				=> 'application/octet-stream',
				'so'				=> 'application/octet-stream',
				'dll'				=> 'application/octet-stream',
				'img'				=> 'application/octet-stream',
				'iso'				=> 'application/octet-stream',
				'oda'				=> 'application/oda',
				'opf'				=> 'application/oebps-package+xml',
				'ogx'				=> 'application/ogg',
				'pdf'				=> 'application/pdf',
				'sig'				=> 'application/pgp-signature',
				'p10'				=> 'application/pkcs10',
				'p7m'				=> 'application/pkcs7-mime',
				'p7c'				=> 'application/pkcs7-mime',
				'p7s'				=> 'application/pkcs7-signature',
				'cer'				=> 'application/pkix-cert',
				'crl'				=> 'application/pkix-crl',
				'pkipath'			=> 'application/pkix-pkipath',
				'pls'				=> 'application/pls+xml',
				'ai'				=> 'application/postscript',
				'eps'				=> 'application/postscript',
				'ps'				=> 'application/postscript',
				'cw'				=> 'application/prs.cww',
				'cww'				=> 'application/prs.cww',
				'rnd'				=> 'application/prs.nprend',
				'rct'				=> 'application/prs.nprend',
				'rdf'				=> 'application/rdf+xml',
				'rif'				=> 'application/reginfo+xml',
				'rnc'				=> 'application/relax-ng-compact-syntax',
				'rld'				=> 'application/resource-lists-diff+xml',
				'rl'				=> 'application/resource-lists+xml',
				'rs'				=> 'application/rls-services+xml',
				'rtf'				=> 'application/rtf',
				'scq'				=> 'application/scvp-cv-request',
				'scs'				=> 'application/scvp-cv-response',
				'spq'				=> 'application/scvp-vp-request',
				'spp'				=> 'application/scvp-vp-response',
				'sdp'				=> 'application/sdp',
				'soc'				=> 'application/sgml-open-catalog',
				'shf'				=> 'application/shf+xml',
				'siv'				=> 'application/sieve',
				'sieve'				=> 'application/sieve',
				'cl'				=> 'application/simple-filter+xml',
				'smil'				=> 'application/smil',
				'smi'				=> 'application/smil',
				'sml'				=> 'application/smil',
				'rq'				=> 'application/sparql-query',
				'srx'				=> 'application/sparql-results+xml',
				'gram'				=> 'application/srgs',
				'grxml'				=> 'application/srgs+xml',
				'ssml'				=> 'application/ssml+xml',
				'tsq'				=> 'application/timestamp-query',
				'tsr'				=> 'application/timestamp-reply',
				'plb'				=> 'application/vnd.3gpp.pic-bw-large',
				'psb'				=> 'application/vnd.3gpp.pic-bw-small',
				'pvb'				=> 'application/vnd.3gpp.pic-bw-var',
				'sms'				=> 'application/vnd.3gpp2.sms',
				'tcap'				=> 'application/vnd.3gpp2.tcap',
				'pwn'				=> 'application/vnd.3M.Post-it-Notes',
				'aso'				=> 'application/vnd.accpac.simply.aso',
				'imp'				=> 'application/vnd.accpac.simply.imp',
				'acu'				=> 'application/vnd.acucobol',
				'atc'				=> 'application/vnd.acucorp',
				'acutc'				=> 'application/vnd.acucorp',
				'xdp'				=> 'application/vnd.adobe.xdp+xml',
				'xfdf'				=> 'application/vnd.adobe.xfdf',
				'azf'				=> 'application/vnd.airzip.filesecure.azf',
				'azs'				=> 'application/vnd.airzip.filesecure.azs',
				'acc'				=> 'application/vnd.americandynamics.acc',
				'ami'				=> 'application/vnd.amiga.ami',
				'cii'				=> 'application/vnd.anser-web-certificate-issue-initiation',
				'fti'				=> 'application/vnd.anser-web-funds-transfer-initiation',
				'dist'				=> 'application/vnd.apple.installer+xml',
				'distz'				=> 'application/vnd.apple.installer+xml',
				'pkg'				=> 'application/vnd.apple.installer+xml',
				'mpkg'				=> 'application/vnd.apple.installer+xml',
				'm3u8'				=> 'application/vnd.apple.mpegurl',
				'swi'				=> 'application/vnd.aristanetworks.swi',
				'aep'				=> 'application/vnd.audiograph',
				'package'			=> 'application/vnd.autopackage',
				'mpm'				=> 'application/vnd.blueice.multipass',
				'ep'				=> 'application/vnd.bluetooth.ep.oob',
				'bmi'				=> 'application/vnd.bmi',
				'rep'				=> 'application/vnd.businessobjects',
				'tlclient'			=> 'application/vnd.cendio.thinlinc.clientconf',
				'cdxml'				=> 'application/vnd.chemdraw+xml',
				'mmd'				=> 'application/vnd.chipnuts.karaoke-mmd',
				'cdy'				=> 'application/vnd.cinderella',
				'cla'				=> 'application/vnd.claymore',
				'rp9'				=> 'application/vnd.cloanto.rp9',
				'c4g'				=> 'application/vnd.clonk.c4group',
				'c4d'				=> 'application/vnd.clonk.c4group',
				'c4f'				=> 'application/vnd.clonk.c4group',
				'c4p'				=> 'application/vnd.clonk.c4group',
				'c4u'				=> 'application/vnd.clonk.c4group',
				'ica'				=> 'application/vnd.commerce-battelle',
				'icf'				=> 'application/vnd.commerce-battelle',
				'icd'				=> 'application/vnd.commerce-battelle',
				'ic0'				=> 'application/vnd.commerce-battelle',
				'ic1'				=> 'application/vnd.commerce-battelle',
				'ic2'				=> 'application/vnd.commerce-battelle',
				'ic3'				=> 'application/vnd.commerce-battelle',
				'ic4'				=> 'application/vnd.commerce-battelle',
				'ic5'				=> 'application/vnd.commerce-battelle',
				'ic6'				=> 'application/vnd.commerce-battelle',
				'ic7'				=> 'application/vnd.commerce-battelle',
				'ic8'				=> 'application/vnd.commerce-battelle',
				'csp'				=> 'application/vnd.commonspace',
				'cst'				=> 'application/vnd.commonspace',
				'cdbcmsg'			=> 'application/vnd.contact.cmsg',
				'cmc'				=> 'application/vnd.cosmocaller',
				'clkx'				=> 'application/vnd.crick.clicker',
				'clkk'				=> 'application/vnd.crick.clicker.keyboard',
				'clkp'				=> 'application/vnd.crick.clicker.palette',
				'clkt'				=> 'application/vnd.crick.clicker.template',
				'clkw'				=> 'application/vnd.crick.clicker.wordbank',
				'wbs'				=> 'application/vnd.criticaltools.wbs+xml',
				'pml'				=> 'application/vnd.ctc-posml',
				'ppd'				=> 'application/vnd.cups-ppd',
				'curl'				=> 'application/vnd.curl',
				'rdz'				=> 'application/vnd.data-vision.rdz',
				'fe_launch'			=> 'application/vnd.denovo.fcselayout-link',
				'dna'				=> 'application/vnd.dna',
				'dpg'				=> 'application/vnd.dpgraph',
				'mwc'				=> 'application/vnd.dpgraph',
				'dpgraph'			=> 'application/vnd.dpgraph',
				'dfac'				=> 'application/vnd.dreamfactory',
				'geo'				=> 'application/vnd.dynageo',
				'mag'				=> 'application/vnd.ecowin.chart',
				'nml'				=> 'application/vnd.enliven',
				'esf'				=> 'application/vnd.epson.esf',
				'msf'				=> 'application/vnd.epson.msf',
				'qam'				=> 'application/vnd.epson.quickanime',
				'slt'				=> 'application/vnd.epson.salt',
				'ssf'				=> 'application/vnd.epson.ssf',
				'qcall'				=> 'application/vnd.ericsson.quickcall',
				'qca'				=> 'application/vnd.ericsson.quickcall',
				'es3'				=> 'application/vnd.eszigno3+xml',
				'et3'				=> 'application/vnd.eszigno3+xml',
				'ez2'				=> 'application/vnd.ezpix-album',
				'ez3'				=> 'application/vnd.ezpix-package',
				'fdf'				=> 'application/vnd.fdf',
				'msd'				=> 'application/vnd.fdsn.mseed',
				'mseed'				=> 'application/vnd.fdsn.mseed',
				'seed'				=> 'application/vnd.fsdn.seed',
				'dataless'			=> 'application/vnd.fsdn.seed',
				'gph'				=> 'application/vnd.FloGraphIt',
				'ftc'				=> 'application/vnd.fluxtime.clip',
				'sfd'				=> 'application/vnd.font-fontforge-sfd',
				'fm'				=> 'application/vnd.framemaker',
				'fnc'				=> 'application/vnd.frogans.fnc',
				'ltf'				=> 'application/vnd.frogans.ltf',
				'fsc'				=> 'application/vnd.fsc.weblaunch',
				'oas'				=> 'application/vnd.fujitsu.oasys',
				'oa2'				=> 'application/vnd.fujitsu.oasys2',
				'oa3'				=> 'application/vnd.fujitsu.oasys3',
				'fg5'				=> 'application/vnd.fujitsu.oasysgp',
				'bh2'				=> 'application/vnd.fujitsu.oasysprs',
				'ddd'				=> 'application/vnd.fujixerox.ddd',
				'xdw'				=> 'application/vnd.fujixerox.docuworks',
				'xbd'				=> 'application/vnd.fujixerox.docuworks.binder',
				'fzs'				=> 'application/vnd.fuzzysheet',
				'txd'				=> 'application/vnd.genomatix.tuxedo',
				'ggb'				=> 'application/vnd.geogebra.file',
				'ggt'				=> 'application/vnd.geogebra.tool',
				'gex'				=> 'application/vnd.geometry-explorer',
				'gre'				=> 'application/vnd.geometry-explorer',
				'gxt'				=> 'application/vnd.geonext',
				'g2w'				=> 'application/vnd.geoplan',
				'g3w'				=> 'application/vnd.geospace',
				'kml'				=> 'application/vnd.google-earth.kml+xml',
				'kmz'				=> 'application/vnd.google-earth.kmz',
				'gqf'				=> 'application/vnd.grafeq',
				'gqs'				=> 'application/vnd.grafeq',
				'gac'				=> 'application/vnd.groove-account',
				'ghf'				=> 'application/vnd.groove-help',
				'gim'				=> 'application/vnd.groove-identity-message',
				'grv'				=> 'application/vnd.groove-injector',
				'gtm'				=> 'application/vnd.groove-tool-message',
				'tpl'				=> 'application/vnd.groove-tool-template',
				'vcg'				=> 'application/vnd.groove-vcard',
				'zmm'				=> 'application/vnd.HandHeld-Entertainment+xml',
				'hbci'				=> 'application/vnd.hbci',
				'hbc'				=> 'application/vnd.hbci',
				'kom'				=> 'application/vnd.hbci',
				'upa'				=> 'application/vnd.hbci',
				'pkd'				=> 'application/vnd.hbci',
				'bpd'				=> 'application/vnd.hbci',
				'les'				=> 'application/vnd.hhe.lesson-player',
				'hpgl'				=> 'application/vnd.hp-HPGL',
				'hpi'				=> 'application/vnd.hp-hpid',
				'hpid'				=> 'application/vnd.hp-hpid',
				'hps'				=> 'application/vnd.hp-hps',
				'jlt'				=> 'application/vnd.hp-jlyt',
				'pcl'				=> 'application/vnd.hp-PCL',
				'sfd-hdstx'			=> 'application/vnd.hydrostatix.sof-data',
				'x3d'				=> 'application/vnd.hzn-3d-crossword',
				'emm'				=> 'application/vnd.ibm.electronic-media',
				'mpy'				=> 'application/vnd.ibm.MiniPay',
				'list3820'			=> 'application/vnd.ibm.modcap',
				'listafp'			=> 'application/vnd.ibm.modcap',
				'afp'				=> 'application/vnd.ibm.modcap',
				'pseg3820'			=> 'application/vnd.ibm.modcap',
				'irm'				=> 'application/vnd.ibm.rights-management',
				'sc'				=> 'application/vnd.ibm.secure-container',
				'icc'				=> 'application/vnd.iccprofile',
				'icm'				=> 'application/vnd.iccprofile',
				'igl'				=> 'application/vnd.igloader',
				'ivp'				=> 'application/vnd.immervision-ivp',
				'ivu'				=> 'application/vnd.immervision-ivu',
				'xpw'				=> 'application/vnd.intercon.formnet',
				'xpx'				=> 'application/vnd.intercon.formnet',
				'qbo'				=> 'application/vnd.intu.qbo',
				'qfx'				=> 'application/vnd.intu.qfx',
				'rcprofile'			=> 'application/vnd.ipunplugged.rcprofile',
				'irp'				=> 'application/vnd.irepository.package+xml',
				'xpr'				=> 'application/vnd.is-xpr',
				'jam'				=> 'application/vnd.jam',
				'rms'				=> 'application/vnd.jcp.javame.midlet-rms',
				'jisp'				=> 'application/vnd.jisp',
				'joda'				=> 'application/vnd.joost.joda-archive',
				'ktz'				=> 'application/vnd.kahootz',
				'ktr'				=> 'application/vnd.kahootz',
				'karbon'			=> 'application/vnd.kde.karbon',
				'chrt'				=> 'application/vnd.kde.kchart',
				'kfo'				=> 'application/vnd.kde.kformula',
				'flw'				=> 'application/vnd.kde.kivio',
				'kon'				=> 'application/vnd.kde.kontour',
				'kpr'				=> 'application/vnd.kde.kpresenter',
				'kpt'				=> 'application/vnd.kde.kpresenter',
				'ksp'				=> 'application/vnd.kde.kspread',
				'kwd'				=> 'application/vnd.kde.kword',
				'kwt'				=> 'application/vnd.kde.kword',
				'htke'				=> 'application/vnd.kenameaapp',
				'kia'				=> 'application/vnd.kidspiration',
				'kne'				=> 'application/vnd.Kinar',
				'knp'				=> 'application/vnd.Kinar',
				'sdf'				=> 'application/vnd.Kinar',
				'skp'				=> 'application/vnd.koan',
				'skd'				=> 'application/vnd.koan',
				'skm'				=> 'application/vnd.koan',
				'skt'				=> 'application/vnd.koan',
				'sse'				=> 'application/vnd.kodak-descriptor',
				'lbd'				=> 'application/vnd.llamagraphics.life-balance.desktop',
				'lbe'				=> 'application/vnd.llamagraphics.life-balance.exchange+xml',
				//123					=> 'application/vnd.lotus-1-2-3',
				'wk4'				=> 'application/vnd.lotus-1-2-3',
				'wk3'				=> 'application/vnd.lotus-1-2-3',
				'wk1'				=> 'application/vnd.lotus-1-2-3',
				'apr'				=> 'application/vnd.lotus-approach',
				'vew'				=> 'application/vnd.lotus-approach',
				'prz'				=> 'application/vnd.lotus-freelance',
				'pre'				=> 'application/vnd.lotus-freelance',
				'nsf'				=> 'application/vnd.lotus-notes',
				'ntf'				=> 'application/vnd.lotus-notes',
				'ndl'				=> 'application/vnd.lotus-notes',
				'ns4'				=> 'application/vnd.lotus-notes',
				'ns3'				=> 'application/vnd.lotus-notes',
				'ns2'				=> 'application/vnd.lotus-notes',
				'nsh'				=> 'application/vnd.lotus-notes',
				'nsg'				=> 'application/vnd.lotus-notes',
				'or3'				=> 'application/vnd.lotus-organizer',
				'or2'				=> 'application/vnd.lotus-organizer',
				'org'				=> 'application/vnd.lotus-organizer',
				'scm'				=> 'application/vnd.lotus-screencam',
				'lwp'				=> 'application/vnd.lotus-wordpro',
				'sam'				=> 'application/vnd.lotus-wordpro',
				'portpkg'			=> 'application/vnd.macports.portpkg',
				'mdc'				=> 'application/vnd.marlin.drm.mdcf',
				'mcd'				=> 'application/vnd.mcd',
				'mc1'				=> 'application/vnd.medcalcdata',
				'cdkey'				=> 'application/vnd.mediastation.cdkey',
				'mwf'				=> 'application/vnd.MFER',
				'mfm'				=> 'application/vnd.mfmp',
				'flo'				=> 'application/vnd.micrografx.flo',
				'igx'				=> 'application/vnd.micrografx.igx',
				'mif'				=> 'application/vnd.mif',
				'daf'				=> 'application/vnd.Mobius.DAF',
				'dis'				=> 'application/vnd.Mobius.DIS',
				'mbk'				=> 'application/vnd.Mobius.MBK',
				'mqy'				=> 'application/vnd.Mobius.MQY',
				'msl'				=> 'application/vnd.Mobius.MSL',
				'plc'				=> 'application/vnd.Mobius.PLC',
				'txf'				=> 'application/vnd.Mobius.TXF',
				'mpn'				=> 'application/vnd.mophun.application',
				'mpc'				=> 'application/vnd.mophun.certificate',
				'xul'				=> 'application/vnd.mozilla.xul+xml',
				'cil'				=> 'application/vnd.ms-artgalry',
				'asf'				=> 'application/vnd.ms-asf',
				'cab'				=> 'application/vnd.ms-cab-compressed',
				'xls'				=> 'application/vnd.ms-excel',
				'eot'				=> 'application/vnd.ms-fontobject',
				'chm'				=> 'application/vnd.ms-htmlhelp',
				'ims'				=> 'application/vnd.ms-ims',
				'lrm'				=> 'application/vnd.ms-lrm',
				'ppt'				=> 'application/vnd.ms-powerpoint',
				'mpp'				=> 'application/vnd.ms-project',
				'tnef'				=> 'application/vnd.ms-tnef',
				'tnf'				=> 'application/vnd.ms-tnef',
				'wcm'				=> 'application/vnd.ms-works',
				'wdb'				=> 'application/vnd.ms-works',
				'wks'				=> 'application/vnd.ms-works',
				'wps'				=> 'application/vnd.ms-works',
				'wpl'				=> 'application/vnd.ms-wpl',
				'xps'				=> 'application/vnd.ms-xpsdocument',
				'mseq'				=> 'application/vnd.mseq',
				'crtr'				=> 'application/vnd.multiad.creator',
				'cif'				=> 'application/vnd.multiad.creator.cif',
				'mus'				=> 'application/vnd.musician',
				'msty'				=> 'application/vnd.muvee.style',
				'entity'			=> 'application/vnd.nervana',
				'request'			=> 'application/vnd.nervana',
				'bkm'				=> 'application/vnd.nervana',
				'kcm'				=> 'application/vnd.nervana',
				'nlu'				=> 'application/vnd.neurolanguage.nlu',
				'nnd'				=> 'application/vnd.noblenet-directory',
				'nns'				=> 'application/vnd.noblenet-sealer',
				'nnw'				=> 'application/vnd.noblenet-web',
				'ac'				=> 'application/vnd.nokia.n-gage.ac+xml',
				'ngdat'				=> 'application/vnd.nokia.n-gage.data',
				'n-gage'			=> 'application/vnd.nokia.n-gage.symbian.install',
				'rpst'				=> 'application/vnd.nokia.radio-preset',
				'rpss'				=> 'application/vnd.nokia.radio-presets',
				'edm'				=> 'application/vnd.novadigm.EDM',
				'edx'				=> 'application/vnd.novadigm.EDX',
				'ext'				=> 'application/vnd.novadigm.EXT',
				'odc'				=> 'application/vnd.oasis.opendocument.chart',
				'otc'				=> 'application/vnd.oasis.opendocument.chart-template',
				'odb'				=> 'application/vnd.oasis.opendocument.database',
				'odf'				=> 'application/vnd.oasis.opendocument.formula',
				'otf'				=> 'application/vnd.oasis.opendocument.formula-template',
				'odg'				=> 'application/vnd.oasis.opendocument.graphics',
				'otg'				=> 'application/vnd.oasis.opendocument.graphics-template',
				'odi'				=> 'application/vnd.oasis.opendocument.image',
				'oti'				=> 'application/vnd.oasis.opendocument.image-template',
				'odp'				=> 'application/vnd.oasis.opendocument.presentation',
				'otp'				=> 'application/vnd.oasis.opendocument.presentation-template',
				'ods'				=> 'application/vnd.oasis.opendocument.spreadsheet',
				'ots'				=> 'application/vnd.oasis.opendocument.spreadsheet-template',
				'odt'				=> 'application/vnd.oasis.opendocument.text',
				'odm'				=> 'application/vnd.oasis.opendocument.text-master',
				'ott'				=> 'application/vnd.oasis.opendocument.text-template',
				'oth'				=> 'application/vnd.oasis.opendocument.text-web',
				'xo'				=> 'application/vnd.olpc-sugar',
				'dd2'				=> 'application/vnd.oma.dd2+xml',
				'oxt'				=> 'application/vnd.openofficeorg.extension',
				'ndc'				=> 'application/vnd.osa.netdeploy',
				'dp'				=> 'application/vnd.osgi.dp',
				'prc'				=> 'application/vnd.palm',
				'pdb'				=> 'application/vnd.palm',
				'pqa'				=> 'application/vnd.palm',
				'oprc'				=> 'application/vnd.palm',
				'str'				=> 'application/vnd.pg.format',
				'ei6'				=> 'application/vnd.pg.osasli',
				'pil'				=> 'application/vnd.piaccess.application-license',
				'efif'				=> 'application/vnd.picsel',
				'plf'				=> 'application/vnd.pocketlearn',
				'pbd'				=> 'application/vnd.powerbuilder6',
				'preminet'			=> 'application/vnd.preminet',
				'box'				=> 'application/vnd.previewsystems.box',
				'vbox'				=> 'application/vnd.previewsystems.box',
				'mgz'				=> 'application/vnd.proteus.magazine',
				'qps'				=> 'application/vnd.publishare-delta-tree',
				'ptid'				=> 'application/vnd.pvi.ptid1',
				'bar'				=> 'application/vnd.qualcomm.brew-app-res',
				'qxd'				=> 'application/vnd.Quark.QuarkXPress',
				'qxt'				=> 'application/vnd.Quark.QuarkXPress',
				'qwd'				=> 'application/vnd.Quark.QuarkXPress',
				'qwt'				=> 'application/vnd.Quark.QuarkXPress',
				'qxl'				=> 'application/vnd.Quark.QuarkXPress',
				'qxb'				=> 'application/vnd.Quark.QuarkXPress',
				'bed'				=> 'application/vnd.realvnc.bed',
				'mxl'				=> 'application/vnd.recordare.musicxml',
				'link66'			=> 'application/vnd.route66.link66+xml',
				'st'				=> 'application/vnd.sailingtracker.track',
				'scd'				=> 'application/vnd.scribus',
				'sla'				=> 'application/vnd.scribus',
				'slaz'				=> 'application/vnd.scribus',
				's3df'				=> 'application/vnd.sealed.3df',
				'scsf'				=> 'application/vnd.sealed.csf',
				'sdoc'				=> 'application/vnd.sealed.doc',
				'sdo'				=> 'application/vnd.sealed.doc',
				's1w'				=> 'application/vnd.sealed.doc',
				'seml'				=> 'application/vnd.sealed.eml',
				'sem'				=> 'application/vnd.sealed.eml',
				'smht'				=> 'application/vnd.sealed.mht',
				'smh'				=> 'application/vnd.sealed.mht',
				'sppt'				=> 'application/vnd.sealed.ppt',
				's1p'				=> 'application/vnd.sealed.ppt',
				'stif'				=> 'application/vnd.sealed.tiff',
				'sxls'				=> 'application/vnd.sealed.xls',
				'sxl'				=> 'application/vnd.sealed.xls',
				's1e'				=> 'application/vnd.sealed.xls',
				'stml'				=> 'application/vnd.sealedmedia.softseal.html',
				's1h'				=> 'application/vnd.sealedmedia.softseal.html',
				'spdf'				=> 'application/vnd.sealedmedia.softseal.pdf',
				'spd'				=> 'application/vnd.sealedmedia.softseal.pdf',
				's1a'				=> 'application/vnd.sealedmedia.softseal.pdf',
				'see'				=> 'application/vnd.seemail',
				'sema'				=> 'application/vnd.sema',
				'semd'				=> 'application/vnd.semd',
				'semf'				=> 'application/vnd.semf',
				'ifm'				=> 'application/vnd.shana.informed.formdata',
				'itp'				=> 'application/vnd.shana.informed.formtemplate',
				'iif'				=> 'application/vnd.shana.informed.interchange',
				'ipk'				=> 'application/vnd.shana.informed.package',
				'twd'				=> 'application/vnd.SimTech-MindMapper',
				'twds'				=> 'application/vnd.SimTech-MindMapper',
				'mmf'				=> 'application/vnd.smaf',
				'teacher'			=> 'application/vnd.smart.teacher',
				'fo'				=> 'application/vnd.software602.filler.form+xml',
				'zfo'				=> 'application/vnd.software602.filler.form-xml-zip',
				'sdkm'				=> 'application/vnd.solent.sdkm+xml',
				'sdkd'				=> 'application/vnd.solent.sdkm+xml',
				'dxp'				=> 'application/vnd.spotfire.dxp',
				'sfs'				=> 'application/vnd.spotfire.sfs',
				'wadl'				=> 'application/vnd.sun.wadl+xml',
				'sus'				=> 'application/vnd.sus-calendar',
				'susp'				=> 'application/vnd.sus-calendar',
				'bdm'				=> 'application/vnd.syncml.dm+wbxml',
				'xdm'				=> 'application/vnd.syncml.dm+xml',
				'xsm'				=> 'application/vnd.syncml+xml',
				'tao'				=> 'application/vnd.tao.intent-module-archive',
				'tmo'				=> 'application/vnd.tmobile-livetv',
				'tpt'				=> 'application/vnd.trid.tpt',
				'mxs'				=> 'application/vnd.triscape.mxs',
				'tra'				=> 'application/vnd.trueapp',
				'ufdl'				=> 'application/vnd.ufdl',
				'ufd'				=> 'application/vnd.ufdl',
				'frm'				=> 'application/vnd.ufdl',
				'utz'				=> 'application/vnd.uiq.theme',
				'umj'				=> 'application/vnd.umajin',
				'unityweb'			=> 'application/vnd.unity',
				'uoml'				=> 'application/vnd.uoml+xml',
				'uo'				=> 'application/vnd.uoml+xml',
				'vcx'				=> 'application/vnd.vcx',
				'mxi'				=> 'application/vnd.vd-study',
				'study-inter'		=> 'application/vnd.vd-study',
				'model-inter'		=> 'application/vnd.vd-study',
				'vwx'				=> 'application/vnd.vectorworks',
				'vsc'				=> 'application/vnd.vidsoft.vidconference',
				'vsd'				=> 'application/vnd.visio',
				'vst'				=> 'application/vnd.visio',
				'vsw'				=> 'application/vnd.visio',
				'vss'				=> 'application/vnd.visio',
				'vis'				=> 'application/vnd.visionary',
				'vsf'				=> 'application/vnd.vsf',
				'sic'				=> 'application/vnd.wap.sic',
				'slc'				=> 'application/vnd.wap.slc',
				'wbxml'				=> 'application/vnd.wap.wbxml',
				'wmlc'				=> 'application/vnd.wap.wmlc',
				'wmlsc'				=> 'application/vnd.wap.wmlscriptc',
				'wtb'				=> 'application/vnd.webturbo',
				'wsc'				=> 'application/vnd.wfa.wsc',
				'wmc'				=> 'application/vnd.wmc',
				'm'					=> 'application/vnd.wolfram.mathematica.package',
				'nbp'				=> 'application/vnd.wolfram.player',
				'wpd'				=> 'application/vnd.wordperfect',
				'wqd'				=> 'application/vnd.wqd',
				'stf'				=> 'application/vnd.wt.stf',
				'wv'				=> 'application/vnd.wv.csp+wbxml',
				'xar'				=> 'application/vnd.xara',
				'xfdl'				=> 'application/vnd.xfdl',
				'xfd'				=> 'application/vnd.xfdl',
				'cpkg'				=> 'application/vnd.xmpie.cpkg',
				'dpkg'				=> 'application/vnd.xmpie.dpkg',
				'ppkg'				=> 'application/vnd.xmpie.ppkg',
				'xlim'				=> 'application/vnd.xmpie.xlim',
				'hvd'				=> 'application/vnd.yamaha.hv-dic',
				'hvs'				=> 'application/vnd.yamaha.hv-script',
				'hvp'				=> 'application/vnd.yamaha.hv-voice',
				'osf'				=> 'application/vnd.yamaha.openscoreformat',
				'saf'				=> 'application/vnd.yamaha.smaf-audio',
				'spf'				=> 'application/vnd.yamaha.smaf-phrase',
				'cmp'				=> 'application/vnd.yellowriver-custom-menu',
				'zir'				=> 'application/vnd.zul',
				'zirz'				=> 'application/vnd.zul',
				'zaz'				=> 'application/vnd.zzazz.deck+xml',
				'vxml'				=> 'application/voicexml+xml',
				'wif'				=> 'application/watcherinfo+xml',
				'wsdl'				=> 'application/wsdl+xml',
				'wspolicy'			=> 'application/wspolicy+xml',
				'xav'				=> 'application/xcap-att+xml',
				'xca'				=> 'application/xcap-caps+xml',
				'xel'				=> 'application/xcap-el+xml',
				'xer'				=> 'application/xcap-error+xml',
				'xns'				=> 'application/xcap-ns+xml',
				'xhtml'				=> 'application/xhtml+xml',
				'xhtm'				=> 'application/xhtml+xml',
				'xht'				=> 'application/xhtml+xml',
				'dtd'				=> 'application/xml-dtd',
				'xop'				=> 'application/xop+xml',
				'xsl'				=> 'application/xslt+xml',
				'xslt'				=> 'application/xslt+xml',
				'mxml'				=> 'application/xv+xml',
				'xhvml'				=> 'application/xv+xml',
				'xvml'				=> 'application/xv+xml',
				'xvm'				=> 'application/xv+xml',
				'zip'				=> 'application/zip',
				//726					=> 'audio/32kadpcm',
				'ac3'				=> 'audio/ac3',
				'amr'				=> 'audio/AMR',
				'awb'				=> 'audio/AMR-WB',
				'aal'				=> 'audio/ATRAC-ADVANCED-LOSSLESS',
				'atx'				=> 'audio/ATRAC-X',
				'at3'				=> 'audio/ATRAC3',
				'aa3'				=> 'audio/ATRAC3',
				'omg'				=> 'audio/ATRAC3',
				'au'				=> 'audio/basic',
				'snd'				=> 'audio/basic',
				'dls'				=> 'audio/dls',
				'evc'				=> 'audio/EVRC',
				'evb'				=> 'audio/EVRCB',
				'evw'				=> 'audio/EVRCWB',
				'lbc'				=> 'audio/iLBC',
				'l16'				=> 'audio/L16',
				'mxmf'				=> 'audio/mobile-xmf',
				'mpga'				=> 'audio/mpeg',
				'mp1'				=> 'audio/mpeg',
				'mp2'				=> 'audio/mpeg',
				'mp3'				=> 'audio/mpeg',
				'oga'				=> 'audio/ogg',
				'ogg'				=> 'audio/ogg',
				'spx'				=> 'audio/ogg',
				'sid'				=> 'audio/prs.sid',
				'psid'				=> 'audio/prs.sid',
				'qcp'				=> 'audio/qcelp',
				'smv'				=> 'audio/SMV',
				'koz'				=> 'audio/vnd.audikoz',
				'eol'				=> 'audio/vnd.digital-winds',
				'mlp'				=> 'audio/vnd.dolby.mlp',
				'dts'				=> 'audio/vnd.dts',
				'dtshd'				=> 'audio/vnd.dts.hd',
				'plj'				=> 'audio/vnd.everad.plj',
				'lvp'				=> 'audio/vnd.lucent.voice',
				'pya'				=> 'audio/vnd.ms-playready.media.pya',
				'vbk'				=> 'audio/vnd.nortel.vbk',
				'ecelp4800'			=> 'audio/vnd.nuera.ecelp4800',
				'ecelp7470'			=> 'audio/vnd.nuera.ecelp7470',
				'ecelp9600'			=> 'audio/vnd.nuera.ecelp9600',
				'smp3'				=> 'audio/vnd.sealedmedia.softseal.mpeg',
				'smp'				=> 'audio/vnd.sealedmedia.softseal.mpeg',
				's1m'				=> 'audio/vnd.sealedmedia.softseal.mpeg',
				'fits'				=> 'image/fits',
				'fit'				=> 'image/fits',
				'fts'				=> 'image/fits',
				'gif'				=> 'image/gif',
				'ief'				=> 'image/ief',
				'jp2'				=> 'image/jp2',
				'jpg2'				=> 'image/jp2',
				'jpeg'				=> 'image/jpeg',
				'jpg'				=> 'image/jpeg',
				'jpe'				=> 'image/jpeg',
				'jfif'				=> 'image/jpeg',
				'jpm'				=> 'image/jpm',
				'jpgm'				=> 'image/jpm',
				'jpx'				=> 'image/jpx',
				'jpf'				=> 'image/jpx',
				'png'				=> 'image/png',
				'btif'				=> 'image/prs.btif',
				'btf'				=> 'image/prs.btif',
				'pti'				=> 'image/prs.pti',
				't38'				=> 'image/t38',
				'tiff'				=> 'image/tiff',
				'tif'				=> 'image/tiff',
				'tfx'				=> 'image/tiff-fx',
				'psd'				=> 'image/vnd.adobe.photoshop',
				'djvu'				=> 'image/vnd.djvu',
				'djv'				=> 'image/vnd.djvu',
				'dxf'				=> 'image/vnd.dxf',
				'fbs'				=> 'image/vnd.fastbidsheet',
				'fpx'				=> 'image/vnd.fpx',
				'fst'				=> 'image/vnd.fst',
				'mmr'				=> 'image/vnd.fujixerox.edmics-mmr',
				'rlc'				=> 'image/vnd.fujixerox.edmics-rlc',
				'pgb'				=> 'image/vnd.globalgraphics.pgb',
				'ico'				=> 'image/vnd.microsoft.icon',
				'mdi'				=> 'image/vnd.ms-modi',
				'hdr'				=> 'image/vnd.radiance',
				'rgbe'				=> 'image/vnd.radiance',
				'xyze'				=> 'image/vnd.radiance',
				'spng'				=> 'image/vnd.sealed.png',
				'spn'				=> 'image/vnd.sealed.png',
				's1n'				=> 'image/vnd.sealed.png',
				'sgif'				=> 'image/vnd.sealedmedia.softseal.gif',
				'sgi'				=> 'image/vnd.sealedmedia.softseal.gif',
				's1g'				=> 'image/vnd.sealedmedia.softseal.gif',
				'sjpg'				=> 'image/vnd.sealedmedia.softseal.jpg',
				'sjp'				=> 'image/vnd.sealedmedia.softseal.jpg',
				's1j'				=> 'image/vnd.sealedmedia.softseal.jpg',
				'wbmp'				=> 'image/vnd.wap.wbmp',
				'xif'				=> 'image/vnd.xiff',
				'u8msg'				=> 'message/global',
				'u8dsn'				=> 'message/global-delivery-status',
				'u8mdn'				=> 'message/global-disposition-notification',
				'u8hdr'				=> 'message/global-headers',
				'eml'				=> 'message/rfc822',
				'mail'				=> 'message/rfc822',
				'art'				=> 'message/rfc822',
				'igs'				=> 'model/iges',
				'iges'				=> 'model/iges',
				'msh'				=> 'model/mesh',
				'mesh'				=> 'model/mesh',
				'silo'				=> 'model/mesh',
				'dwf'				=> 'model/vnd.dwf',
				'gdl'				=> 'model/vnd.gdl',
				'gsm'				=> 'model/vnd.gdl',
				'win'				=> 'model/vnd.gdl',
				'dor'				=> 'model/vnd.gdl',
				'lmp'				=> 'model/vnd.gdl',
				'rsm'				=> 'model/vnd.gdl',
				'msm'				=> 'model/vnd.gdl',
				'ism'				=> 'model/vnd.gdl',
				'gtw'				=> 'model/vnd.gtw',
				'moml'				=> 'model/vnd.moml+xml',
				'mts'				=> 'model/vnd.mts',
				'x_b'				=> 'model/vnd.parasolid.transmit.binary',
				'xmt_bin'			=> 'model/vnd.parasolid.transmit.binary',
				'x_t'				=> 'model/vnd.parasolid.transmit.text',
				'xmt_txt'			=> 'model/vnd.parasolid.transmit.text',
				'vtu'				=> 'model/vnd.vtu',
				'wrl'				=> 'model/vrml',
				'vrml'				=> 'model/vrml',
				'vpm'				=> 'multipart/voice-message',
				'ics'				=> 'text/calendar',
				'ifb'				=> 'text/calendar',
				'css'				=> 'text/css',
				'csv'				=> 'text/csv',
				'soa'				=> 'text/dns',
				'zone'				=> 'text/dns',
				'html'				=> 'text/html',
				'htm'				=> 'text/html',
				'js'				=> 'text/javascript',
				'asc'				=> 'text/plain',
				'txt'				=> 'text/plain',
				'text'				=> 'text/plain',
				'pm'				=> 'text/plain',
				'el'				=> 'text/plain',
				'c'					=> 'text/plain',
				'h'					=> 'text/plain',
				'cc'				=> 'text/plain',
				'hh'				=> 'text/plain',
				'cxx'				=> 'text/plain',
				'hxx'				=> 'text/plain',
				'f90'				=> 'text/plain',
				'rst'				=> 'text/prs.fallenstein.rst',
				'tag'				=> 'text/prs.lines.tag',
				'dsc'				=> 'text/prs.lines.tag',
				'rtx'				=> 'text/richtext',
				'sgml'				=> 'text/sgml',
				'sgm'				=> 'text/sgml',
				'tsv'				=> 'text/tab-separated-values',
				'uris'				=> 'text/uri-list',
				'uri'				=> 'text/uri-list',
				'abc'				=> 'text/vnd.abc',
				'dms'				=> 'text/vnd.DMClientScript',
				'jtd'				=> 'text/vnd.esmertec.theme-descriptor',
				'fly'				=> 'text/vnd.fly',
				'flx'				=> 'text/vnd.fmi.flexstor',
				'gv'				=> 'text/vnd.graphviz',
				'dot'				=> 'text/vnd.graphviz',
				'3dml'				=> 'text/vnd.in3d.3dml',
				'3dm'				=> 'text/vnd.in3d.3dml',
				'spot'				=> 'text/vnd.in3d.spot',
				'spo'				=> 'text/vnd.in3d.spot',
				'mpf'				=> 'text/vnd.ms-mediapackage',
				'ccc'				=> 'text/vnd.net2phone.commcenter.command',
				'uric'				=> 'text/vnd.si.uricatalogue',
				'jad'				=> 'text/vnd.sun.j2me.app-descriptor',
				'ts'				=> 'text/vnd.trolltech.linguist',
				'si'				=> 'text/vnd.wap.si',
				'sl'				=> 'text/vnd.wap.sl',
				'wml'				=> 'text/vnd.wap.wml',
				'wmls'				=> 'text/vnd.wap.wmlscript',
				'xml'				=> 'text/xml',
				'ent'				=> 'text/xml-external-parsed-entity',
				'3gp'				=> 'video/3gpp',
				'3gpp'				=> 'video/3gpp',
				'3g2'				=> 'video/3gpp2',
				'3gpp2'				=> 'video/3gpp2',
				'mj2'				=> 'video/mj2',
				'mjp2'				=> 'video/mj2',
				'mp4'				=> 'video/mp4',
				'mpg4'				=> 'video/mp4',
				'mpeg'				=> 'video/mpeg',
				'mpg'				=> 'video/mpeg',
				'mpe'				=> 'video/mpeg',
				'ogv'				=> 'video/ogg',
				'qt'				=> 'video/quicktime',
				'mov'				=> 'video/quicktime',
				'fvt'				=> 'video/vnd.fvt',
				'mxu'				=> 'video/vnd.mpegurl',
				'm4u'				=> 'video/vnd.mpegurl',
				'pyv'				=> 'video/vnd.ms-playready.media.pyv',
				'nim'				=> 'video/vnd.nokia.interleaved-multimedia',
				'smpg'				=> 'video/vnd.sealed.mpeg1',
				's11'				=> 'video/vnd.sealed.mpeg1',
				's14'				=> 'video/vnd.sealed.mpeg4',
				'sswf'				=> 'video/vnd.sealed.swf',
				'ssw'				=> 'video/vnd.sealed.swf',
				'smov'				=> 'video/vnd.sealedmedia.softseal.mov',
				'smo'				=> 'video/vnd.sealedmedia.softseal.mov',
				's1q'				=> 'video/vnd.sealedmedia.softseal.mov',
				'cpt'				=> 'application/mac-compactpro',
				'mml'				=> 'application/mathml+xml',
				'metalink'			=> 'application/metalink+xml',
				'rss'				=> 'application/rss+xml',
				'xlam'				=> 'application/vnd.ms-excel.addin.macroEnabled.12',
				'xlsb'				=> 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
				'xlsm'				=> 'application/vnd.ms-excel.sheet.macroEnabled.12',
				'xltm'				=> 'application/vnd.ms-excel.template.macroEnabled.12',
				'ppam'				=> 'application/vnd.ms-powerpoint.addin.macroEnabled.12',
				'pptm'				=> 'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
				'sldm'				=> 'application/vnd.ms-powerpoint.slide.macroEnabled.12',
				'ppsm'				=> 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
				'potm'				=> 'application/vnd.ms-powerpoint.template.macroEnabled.12',
				'docm'				=> 'application/vnd.ms-word.document.macroEnabled.12',
				'dotm'				=> 'application/vnd.ms-word.template.macroEnabled.12',
				'dd'				=> 'application/vnd.oma.dd+xml',
				'dcf'				=> 'application/vnd.oma.drm.content',
				'o4a'				=> 'application/vnd.oma.drm.dcf',
				'o4v'				=> 'application/vnd.oma.drm.dcf',
				'dm'				=> 'application/vnd.oma.drm.message',
				'drc'				=> 'application/vnd.oma.drm.rights+wbxml',
				'dr'				=> 'application/vnd.oma.drm.rights+xml',
				'pptx'				=> 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
				'sldx'				=> 'application/vnd.openxmlformats-officedocument.presentationml.slide',
				'ppsx'				=> 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
				'potx'				=> 'application/vnd.openxmlformats-officedocument.presentationml.template',
				'xlsx'				=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
				'xltx'				=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
				'docx'				=> 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
				'dotx'				=> 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
				'sxc'				=> 'application/vnd.sun.xml.calc',
				'stc'				=> 'application/vnd.sun.xml.calc.template',
				'sxd'				=> 'application/vnd.sun.xml.draw',
				'std'				=> 'application/vnd.sun.xml.draw.template',
				'sxi'				=> 'application/vnd.sun.xml.impress',
				'sti'				=> 'application/vnd.sun.xml.impress.template',
				'sxm'				=> 'application/vnd.sun.xml.math',
				'sxw'				=> 'application/vnd.sun.xml.writer',
				'sxg'				=> 'application/vnd.sun.xml.writer.global',
				'stw'				=> 'application/vnd.sun.xml.writer.template',
				'sis'				=> 'application/vnd.symbian.install',
				'mms'				=> 'application/vnd.wap.mms-message',
				'bcpio'				=> 'application/x-bcpio',
				'torrent'			=> 'application/x-bittorrent',
				'bz2'				=> 'application/x-bzip2',
				'vcd'				=> 'application/x-cdlink',
				'pgn'				=> 'application/x-chess-pgn',
				'cpio'				=> 'application/x-cpio',
				'csh'				=> 'application/x-csh',
				'dcr'				=> 'application/x-director',
				'dir'				=> 'application/x-director',
				'dxr'				=> 'application/x-director',
				'dvi'				=> 'application/x-dvi',
				'spl'				=> 'application/x-futuresplash',
				'gtar'				=> 'application/x-gtar',
				'gz'				=> 'application/x-gzip',
				'tgz'				=> 'application/x-gzip',
				'hdf'				=> 'application/x-hdf',
				'jar'				=> 'application/x-java-archive',
				'jnlp'				=> 'application/x-java-jnlp-file',
				'pack'				=> 'application/x-java-pack200',
				'kil'				=> 'application/x-killustrator',
				'latex'				=> 'application/x-latex',
				'nc'				=> 'application/x-netcdf',
				'cdf'				=> 'application/x-netcdf',
				'pl'				=> 'application/x-perl',
				'rpm'				=> 'application/x-rpm',
				'sh'				=> 'application/x-sh',
				'shar'				=> 'application/x-shar',
				'swf'				=> 'application/x-shockwave-flash',
				'sit'				=> 'application/x-stuffit',
				'sv4cpio'			=> 'application/x-sv4cpio',
				'sv4crc'			=> 'application/x-sv4crc',
				'tar'				=> 'application/x-tar',
				'tcl'				=> 'application/x-tcl',
				'tex'				=> 'application/x-tex',
				'texinfo'			=> 'application/x-texinfo',
				'texi'				=> 'application/x-texinfo',
				't'					=> 'application/x-troff',
				'tr'				=> 'application/x-troff',
				'roff'				=> 'application/x-troff',
				'man'				=> 'application/x-troff-man',
				//1					=> 'application/x-troff-man',
				//2					=> 'application/x-troff-man',
				//3					=> 'application/x-troff-man',
				//4					=> 'application/x-troff-man',
				//5					=> 'application/x-troff-man',
				//6					=> 'application/x-troff-man',
				//7					=> 'application/x-troff-man',
				//8					=> 'application/x-troff-man',
				'me'				=> 'application/x-troff-me',
				'ms'				=> 'application/x-troff-ms',
				'ustar'				=> 'application/x-ustar',
				'src'				=> 'application/x-wais-source',
				'xz'				=> 'application/x-xz',
				'mid'				=> 'audio/midi',
				'midi'				=> 'audio/midi',
				'kar'				=> 'audio/midi',
				'aif'				=> 'audio/x-aiff',
				'aiff'				=> 'audio/x-aiff',
				'aifc'				=> 'audio/x-aiff',
				'mod'				=> 'audio/x-mod',
				'ult'				=> 'audio/x-mod',
				'uni'				=> 'audio/x-mod',
				'm15'				=> 'audio/x-mod',
				'mtm'				=> 'audio/x-mod',
				//669					=> 'audio/x-mod',
				'med'				=> 'audio/x-mod',
				'm3u'				=> 'audio/x-mpegurl',
				'wax'				=> 'audio/x-ms-wax',
				'wma'				=> 'audio/x-ms-wma',
				'ram'				=> 'audio/x-pn-realaudio',
				'rm'				=> 'audio/x-pn-realaudio',
				'ra'				=> 'audio/x-realaudio',
				's3m'				=> 'audio/x-s3m',
				'stm'				=> 'audio/x-stm',
				'wav'				=> 'audio/x-wav',
				'xyz'				=> 'chemical/x-xyz',
				'bmp'				=> 'image/bmp',
				'svg'				=> 'image/svg+xml',
				'svgz'				=> 'image/svg+xml',
				'ras'				=> 'image/x-cmu-raster',
				'pnm'				=> 'image/x-portable-anymap',
				'pbm'				=> 'image/x-portable-bitmap',
				'pgm'				=> 'image/x-portable-graymap',
				'ppm'				=> 'image/x-portable-pixmap',
				'rgb'				=> 'image/x-rgb',
				'tga'				=> 'image/x-targa',
				'xbm'				=> 'image/x-xbitmap',
				'xpm'				=> 'image/x-xpixmap',
				'xwd'				=> 'image/x-xwindowdump',
				'manifest'			=> 'text/cache-manifest',
				'pod'				=> 'text/x-pod',
				'etx'				=> 'text/x-setext',
				'vcf'				=> 'text/x-vcard',
				'webm'				=> 'video/webm',
				'flv'				=> 'video/x-flv',
				'asx'				=> 'video/x-ms-asf',
				'wm'				=> 'video/x-ms-wm',
				'wmv'				=> 'video/x-ms-wmv',
				'wmx'				=> 'video/x-ms-wmx',
				'wvx'				=> 'video/x-ms-wvx',
				'avi'				=> 'video/x-msvideo',
				'movie'				=> 'video/x-sgi-movie',
				'ice'				=> 'x-conference/x-cooltalk',
				'sisx'				=> 'x-epoc/x-sisx-app',
			];

			public function __call($name, $arguments = []){

				//GET THE METHOD NAME
				$methodCheck = '_'.$name;

				//CALL THE METHOD
				if(method_exists($this, $methodCheck)) return call_user_func_array([$this, $methodCheck], $arguments);
			}

			public static function __callStatic($name, $arguments = []){

				//GET A NEW VERSION OF THIS CLASS WITHOUT THE CONSTRUCTOR (NEEDED TO BYPASS REQUIRED CONSTRUCTOR VALUES)
				$className 	= '\\'.get_called_class();
				$reflection = new \ReflectionClass($className);
				$obj 		= $reflection->newInstanceWithoutConstructor();

				//MAKE SURE THIS IS INSTANTIATED
				//$obj = new $className;

				//GET THE METHOD NAME
				$methodCheck = '_'.$name;

				//CALL THE METHOD
				if(method_exists($obj, $methodCheck)) return call_user_func_array([$obj, $methodCheck], $arguments);			
			}

			public function __construct($string = null){
				if(!is_null($string)) $this->_parse($string);
			}

			private function _all(){
				return $this->_extensions;
			}

			private function _parse($string){
				if(strpos($string, '.')) $string = pathinfo($string, PATHINFO_EXTENSION);
				$this->_raw = $string;
				if(!is_null($this->_raw)){
					$string = trim(strtolower($string));
					if(isset($this->_extensions[$string])){
						$this->_extVal 	= $string;
						$this->_mimeVal = $this->_extensions[$string];
					}
					elseif($keyVal = array_search($string, $this->_extensions)){
						$this->_extVal = $keyVal;
						$this->_mimeVal = $string;
					}
				}

				return $this;
			}

			public function extension(){
				return $this->_extVal;
			}

			public function type(){
				return $this->_mimeVal;
			}
		}
	}

	if(!function_exists('simpleMail')){
		function simpleMail($to, $subject, $body){
			return new \SimpleMail($to, $subject, $body);
		}
	}

	if(!class_exists('SimpleMail')){
		class SimpleMail {

			private $_headers 		= [];
			private $_from;
			private $_to 			= [];
			private $_replyTo;
			private $_cc 			= [];
			private $_bcc 			= [];
			private $_subject;
			private $_body;
			private $_attachments 	= [];

			public function __construct($to, $subject, $body){
				$this->to($to);
				$this->subject($subject);
				$this->body($body);
			}

			public function from($val, $name = null){
				$this->_from = !is_null($name) ? $name.' <'.$val.'>' : $val;
				return $this;
			}

			public function to($val){
				if(!is_array($val)) $val = [$val];
				$this->_to = $val;
				return $this;
			}

			public function replyTo($val, $name = null){
				$this->_replyTo = !is_null($name) ? $name.' <'.$val.'>' : $val;
				return $this;
			}

			public function cc($val){
				if(!is_array($val)) $val = [$val];
				$this->_cc = $val;
				return $this;
			}

			public function bcc($val){
				if(!is_array($val)) $val = [$val];
				$this->_bcc = $val;
				return $this;
			}

			public function subject($val){
				$this->_subject = $val;
				return $this;
			}

			public function body($val){
				if(is_object($val) && get_class($val) == 'Illuminate\View\View') $this->body = $val->render();
				else $this->_body = $val;
				return $this;
			}

			public function attachments($val){
				if(!is_array($val)) $val = [$val];
				$this->_attachments = $val;
				return $this;
			}

			private function _sendLaravel(){

				//EXTRACT THE OPTIONS TO A SIMPLE OBJECT
                $that = (object)[];
                foreach(['from','to','replyTo','cc','bcc','subject','body','attachments'] as $v) if($vVal = $this->{'_'.$v}) if(!empty($vVal)) $that->$v = $vVal;

                try{
                	//SEND USING LARAVEL MAIL FASCADE
	                \Illuminate\Support\Facades\Mail::send([], [], function($message) use($that){
	                    
	                    //SET RECIPIENT AND SUBJECT LINE
	                    $message->to($that->to)->subject($that->subject);

	                    //SET THE BODY CONTENT (HANDLE LEGACY VERSIONS OF LARAVEL)
	                    try{
	                    	strlen(strip_tags($that->body)) == strlen($that->body) ? $message->text($that->body) : $message->html($that->body);
	                    }
	                    catch(\Throwable $e){
	                    	strlen(strip_tags($that->body)) == strlen($that->body) ? $message->setBody($that->body) : $message->setBody($that->body, 'text/html');
	                    }                

	                    //SET CC RECIPIENTS                    
	                    if(isset($that->cc) && (is_string($that->cc) || is_array($that->cc))) $message->cc((is_string($that->cc) ? explode(',',$that->cc) : $that->cc));

	                    //SET BCC RECIPIENTS
	                    if(isset($that->bcc) && (is_string($that->bcc) || is_array($that->bcc))) $message->bcc((is_string($that->bcc) ? explode(',',$that->bcc) : $that->bcc));

	                    //SET OPTIONAL VALUES
	                    foreach(['from' => 'from', 'replyTo' => 'replyTo'] as $k => $v) if(isset($that->$k) && !empty($that->$k)) $message->$v($that->$k);

	                    if(isset($that->attachments) && is_array($that->attachments) && !empty($that->attachments)){
	                    	foreach($that->attachments as $attachment){
	                    		if(!is_array($attachment)){
	                    			if(file_exists($attachment)) $message->attach($attachment, ['as' => basename($attachment), 'mime' => checkMimeType($attachment)->type()]);
	                    		}
	                    		else{
	                    			$parsedAttachment = $attachment;
	                    			if(!isset($parsedAttachment['content']) && isset($parsedAttachment['path']) && file_exists($parsedAttachment['path'])) $message->attach($parsedAttachment['path'], ['as' => basename($parsedAttachment['path']), 'mime' => checkMimeType($parsedAttachment['path'])->type()]);
	                    			elseif(isset($parsedAttachment['name']) && isset($parsedAttachment['mime']) && isset($parsedAttachment['content'])) $message->attachData($parsedAttachment['content'], $parsedAttachment['name'], ['mime' => $parsedAttachment['mime']]);
	                    		}
	                    	}
	                    }
	                });

	                return true;
                }
                catch(\Throwable $e){
                	//pr($e->getMessage());
                	//px($e->getTraceAsString());
                }

                return false;
			}

			public function send(){
				
				//CHECK IF THE LARAVEL MAIL FACADE EXISTS
				if(class_exists('Illuminate\Support\Facades\Mail')) return $this->_sendLaravel();

				//SET THE BODY CONTENT TYPE
				$bodyContentType = strlen(strip_tags($this->_body)) != strlen($this->_body) ? 'text/html' : 'text/plain';
				
				//$encoded_content 	= chunk_split(base64_encode($outputXML));
				$boundary 			= md5('random');

				//BUILD EMAIL HEADERS
				$headers 			= "";

				if(!empty($this->_from)) $headers 		.= "From: ".$this->_from."\r\n";
				if(!empty($this->_replyTo)) $headers 	.= "Reply-To: ".$this->_replyTo."\r\n";
				if(!empty($this->_cc))  $headers 		.= "CC: ".implode(';',$this->_cc)."\r\n";
				if(!empty($this->_bcc))  $headers 		.= "BCC: ".implode(';',$this->_bcc)."\r\n";

				$headers 			.= "MIME-Version: 1.0\r\n"; // Defining the MIME version
			    $headers 			.= "Content-Type: multipart/mixed;"; // Defining Content-Type
			    $headers 			.= "boundary = $boundary\r\n"; //Defining the Boundary

				//BUILD EMAIL CONTENT BODY
			    $body 				= "--$boundary\r\n";
			    $body 				.= "Content-Type: ".$bodyContentType."; charset=ISO-8859-1\r\n";
			    $body 				.= "Content-Transfer-Encoding: base64\r\n\r\n";
			    $body 				.= chunk_split(base64_encode($this->_body));

			    $parsedAttachments = [];

			    if(isset($this->_attachments) && is_array($this->_attachments) && !empty($this->_attachments)){
			    	foreach($this->_attachments as $attachment){
			    		if(!is_array($attachment)){
			    			if(file_exists($attachment)){

			    				try{
			    					$handle 			= fopen($attachment, "r");
				    				$fileContent 		= fread($handle, filesize($attachment));
				    				fclose($handle);
				    				$encodedFileContent = chunk_split(base64_encode($fileContent));

				    				$parsedAttachments[] = [
				    					'name' 		=> basename($attachment),
				    					'mime' 		=> checkMimeType($attachment)->type(),
				    					'content' 	=> $encodedFileContent,
				    				];
			    				}
			    				catch(\Throwable $e){
			    					//SILENT	
			    				}			    				
			    			}
			    		}
			    		else{
			    			$parsedAttachment = $attachment;
			    			if(!isset($parsedAttachment['content'])){
			    				if(isset($parsedAttachment['path'])){
			    					if(file_exists($parsedAttachment['path'])){

			    						try{
			    							$handle 						= fopen($parsedAttachment['path'], "r");
						    				$fileContent 					= fread($handle, filesize($parsedAttachment['path']));
						    				fclose($handle);
						    				$encodedFileContent 			= chunk_split(base64_encode($fileContent));

						    				$parsedAttachments[] = [
						    					'name' 		=> !isset($attachment['name']) ? basename($parsedAttachment['path']) : $attachment['name'],
						    					'mime' 		=> !isset($attachment['mime']) ? checkMimeType($parsedAttachment['path'])->type() : $attachment['mime'],
						    					'content' 	=> $encodedFileContent,
						    				];				
			    						}
			    						catch(\Throwable $e){
			    							//SILENT
			    						}			
			    					}
			    				}
			    			}
			    			elseif(isset($parsedAttachment['name']) && isset($parsedAttachment['mime']) && isset($parsedAttachment['content'])){
			    				$parsedAttachments[] = $parsedAttachment;
			    			}
			    		}
			    	}
			    }

			    if(!empty($parsedAttachments)) foreach($parsedAttachments as $parsedAttachment){

			    	//BUILD EMAIL ATTACHMENT CONTENT
				    $body 				.= "--$boundary\r\n";
				    $body 				.="Content-Type: ".$parsedAttachment['mime']."; name=".$parsedAttachment['name']."\r\n";
				    $body 				.="Content-Disposition: attachment; filename=".$parsedAttachment['name']."\r\n";
				    $body 				.="Content-Transfer-Encoding: base64\r\n";
				    $body 				.="X-Attachment-Id: ".rand(1000, 99999)."\r\n\r\n";
				    $body 				.= $parsedAttachment['content']; // Attaching the encoded file with email
			    }			    

			    //SEND THE DEBUG EMAIL
				return @mail(implode(',',$this->_to), $this->_subject, $body, $headers);
			}
		}
	}

//---------------------------------- // CLOSURE WRAP //--------------------------------//

	//SERIALIZE A CLOSURE TO A STRING
	if(!function_exists('closure_dump')){
		function closure_dump($func){
			return (string)(new \ClosureWrap($func));
		}
	}

	//WRAP A CLOSURE IN A CLASS AND PARE IF NEEDED
	if(!class_exists('ClosureWrap')){
		class ClosureWrap {
			private $_code;
			private $_closure;

			public function __construct($code){
				if(is_object($code)){
					$this->_code 	= $this->_parse($code);
					$this->_closure = $code;
				}
				else{
					$serializeCheck = @unserialize($code);
					if($serializeCheck !== false) $this->_setClosureFromCode($serializeCheck);
					else $this->_setClosureFromCode($code);
					
				}
			}

			public function __serialize(){
				return [
					'_code' => $this->_code
				];
			}

			public function __unserialize(array $data){
				$this->_setClosureFromCode($data['_code']);
			}

			private function _setClosureFromCode($code){
				$this->_code = $code;
				$tmpFilePath = sys_get_temp_dir().'/closure_'.microtime(true).rand(0,9999).'.php';
				file_put_contents($tmpFilePath, "<?php return ".$code.";");
				$closure = require_once $tmpFilePath;
				unlink($tmpFilePath);
				$this->_closure = $closure;
			}

			//PARSE A CALLBABLE FUNCTION
			private function _parse($func){

				$refl = new \ReflectionFunction($func); // get reflection object
			    $path = $refl->getFileName();  // absolute path of php file
			    $begn = $refl->getStartLine(); // have to `-1` for array index
			    $endn = $refl->getEndLine();
			    $dlim = PHP_EOL;
			    $list = explode($dlim, file_get_contents($path));         // lines of php-file source
			    $list = array_slice($list, ($begn-1), ($endn-($begn-1))); // lines of closure definition
			    $last = (count($list)-1); // last line number

			    if((substr_count($list[0],'function')>1)|| (substr_count($list[0],'{')>1) || (substr_count($list[$last],'}')>1))
			    { throw new \Exception("Too complex context definition in: `$path`. Check lines: $begn & $endn."); }

			    $list[0] = ('function'.explode('function',$list[0])[1]);
			    $list[$last] = (explode('}',$list[$last])[0].'}');

			    //GET THE PARSED FUNCTION
			    $parsedFunction = implode($dlim,$list);

			    //PARSE ANY PASSED VARIABLES
			    $useVars 		= "";
			    if(method_exists($refl, 'getClosureUsedVariables')) if($usedArr = $refl->getClosureUsedVariables()) if(is_array($usedArr) && !empty($usedArr)) foreach($usedArr as $name => $value) $useVars .= '$'.$name.' = '.var_export($value, true).";".PHP_EOL;
			    $newFunction = 'function(...$args){'.PHP_EOL.
			    	$useVars.PHP_EOL.
			    	'$tmpFunction = '.$parsedFunction.';'.PHP_EOL.
			    	'return call_user_func_array($tmpFunction, $args);'.PHP_EOL.
			    "};".PHP_EOL;

			    //SEND BACK THE NEW FUNCTION
			    return $newFunction;
			}

			public function __invoke(...$args){
				return call_user_func_array($this->_closure, $args);
			}

			public function __toString(){
				return $this->_code;
			}

			public function bind($object = null){
				if(is_object($object)) $this->_closure = $this->_closure->bindTo($object);				
				return $this;
			}

			//EXECUTE THE CLOSURE WITH THE ARGUMENTS
			public function call(...$args){			
				return call_user_func_array($this->_closure, $args);
			}

			//GET THE CLOSURE INSTANCE
			public function getClosure(){
				return $this->_closure;
			}
		}
	}


//---------------------------------- // GENERAL OBJECT //--------------------------------//

	//CREATE A GENERAL OBJECT CLASS
	if(!class_exists('GeneralObject')){

		class GeneralObject implements ArrayAccess,JsonSerializable,Countable,Iterator {

			protected $_position = 0;
			protected $_data 	= [];

			public function __construct($data = []){
				$this->_data = (array)$data;
			}

			//--------------------------------------// MAGIC METHODS //--------------------------------------//

			//MAGIC METHOD SET A PARAMETER VALUE
			public function __set($name, $value){
				$this->_data[$name] = $value;
			}

			//MAGIC METHOD RETRIEVE A PARAMETER OR METHOD VALUE BY NAME
			public function __get($name){

				//RETRIEVE THE VALUE OF THE PARAMETER IF IT EXISTS
				if(array_key_exists($name, $this->_data)) return $this->_data[$name];

				//FORWARD TO METHOD IF IT EXISTS
				if(method_exists($this, $name)) return $this->$name();

				//DEFAULT TO RETURN NULL
				return null;
			}

			//MAGIC METHOD CHECK IF A PARAMETER IS SET
			public function __isset($name){
				return isset($this->_data[$name]);
			}

			//MAGIC METHOD REMOVE A PARAMETER VALUE
			public function __unset($name){
				unset($this->_data[$name]);
			}

			//--------------------------------------// Iterator INTERFACE METHODS //--------------------------------------//

			public function rewind(){
				$this->_position = 0;
				return $this;
			}

			public function &current(){
				//return $this->_data[$this->_keys[$this->_position]];
				return $this->_data[$this->_position];
			}

			public function key(){
				//return $this->_keys[$this->_position];
				return $this->_position;
			}

			public function nth($offset){
				return $this->offsetGet($offset);
			}

			public function next(){
				$this->_position ++;
				return $this;
			}

			public function previous(){
				if($this->_position > 0){
					$this->_position --;
				}
				return $this;
			}


			//--------------------------------------// ArrayAccess INTERFACE METHODS //--------------------------------------//

			//SET A _data ARRAY VALUE BY KEY
			public function offsetSet($offset, $value){
				if(is_null($offset)) $this->_data[] = $value;
				else $this->_data[$offset] = $value;
			}

			//CHECK IF A _data ARRAY VALUE EXISTS
			public function offsetExists($offset) {
		        return isset($this->_data[$offset]);
		    }

		    //REMOVE A VALUE FROM THE _data ARRAY
		    public function offsetUnset($offset) {
		        unset($this->_data[$offset]);
		    }

		    //FORWARD ARRAY KEY CALLS TO THE _data ARRAY
		    public function &offsetGet($offset) {
		        return isset($this->_data[$offset]) ? $this->_data[$offset] : null;
		    }

			//--------------------------------------// JsonSerializable INTERFACE METHODS //--------------------------------------//

		    //GENERATE THE DATA FOR JSON
		    public function jsonSerialize() {
		        return $this->_data;
		    }

			//--------------------------------------// Countable INTERFACE METHODS //--------------------------------------//

		    //COUNT THE AMOUNT OF DATA
		    public function count(){
		    	return count(array_filter($this->_data, function($var){
		    		if(is_string($var)) return strlen($var);
		    		if(is_object($var)){
		    			if(is_countable($var)) return count($var);
		    			return 1;
		    		}
		    		return !empty($var);
		    	}));
		    }

			//--------------------------------------// GENERAL METHODS //--------------------------------------//

		    //GET THE DATA AS AN ARRAY
		    public function toArray(){
		    	return $this->_data;
		    }    

		    //CHECK IF THIS OBJECT HAS ANY DATA
		    public function empty(){
		    	return !$this->count();
		    }

		    //PSEUDONIM FOR ->empty()
		    public function exists(){
		    	return !$this->empty();
		    }

		     //PSEUDONIM FOR ->empty()
		    public function valid(){
		    	//return !$this->empty();
		    	return isset($this->_data[$this->_position]);
		    }
		}
	}