<?php

namespace Itul\GlobalFunctions;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Itul\GlobalFunctions\Skeleton\SkeletonClass
 */
class GlobalFunctionsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'itul-global-functions';
    }
}
